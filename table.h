/* table.h - MeowMeow, a stream encoder/decoder */



#define _TABLE_H

#define ENCODER_INIT { "purr", "purR", "puRr", "puRR", \
		       "pUrr", "pUrR", "pURr", "pURR", \
                       "PurR", "PurR", "PuRr", "PuRR", \
		       "PUrr", "PUrR", "PURr", "PURR" }

